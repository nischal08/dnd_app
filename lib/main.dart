import 'package:dnd_app/dnd_packages.dart';
import 'package:dnd_app/real_volume.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primarySwatch: Colors.blueGrey),
      home: DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          bottom:  TabBar(indicatorColor:Colors.white,
            tabs: [
               Tab(icon: Icon(Icons.do_not_disturb), text: 'Flutter Dnd',),
               Tab(icon: Icon(Icons.volume_up), text: 'Real Volume'),
            ],
          ),
          title: const Text("Two Packages"),
          backgroundColor: Colors.teal,
        ),
        body:   TabBarView(
          physics: BouncingScrollPhysics(),
          dragStartBehavior: DragStartBehavior.down,
          children: [
            DndPackages(),
            RealVolumeScreen()
          ],
        ),
      ),
    )
    );
  }
}


